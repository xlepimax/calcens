# Grupo 7. #
# Teoría de compiladores. #

## The best group ever.

### What is this repository for? ###

* Assembly program to operate two numbers.
* Version 1

### What should I know? ###

* R0 hasta R9: Registros de Propósito General.
* A: Almacena el resultado de las operaciones aritméticas y lógicas de dos operandos.
* IX, IY: Se usan para efectuar direccionamientos de memoria.

### Collaborators ###

Leonel Pichardo Martínez
18-0743
lpichardomartinez@gmail.com

Andry Guerrero Yberie
18-0742
andrygtec@gmail.com

Michael Cedano
19-0891
Michael.cedano88@gmail.com

Hiram Arnaud
19-0628
hiram_arnaud@hotmail.com

Luis Enmanuel Carpio
16-0631
ecarpio16@gmail.com

Ángel Beltre
16-0904
angelbciprian@gmail.com
